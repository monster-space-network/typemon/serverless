# [6.0.0-beta.23](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.22...6.0.0-beta.23) (2019-12-16)


### Features

* **runner.dependencies-container:** 컨트롤러, 핸들러 메타데이터 바인딩 추가 ([49329f0](https://gitlab.com/monster-space-network/typemon/serverless/commit/49329f0e08c67850759c6815c5243c8c4c7f518f))



# [6.0.0-beta.22](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.21...6.0.0-beta.22) (2019-12-13)


### Bug Fixes

* **http-runner:** body 잘못된 기본 값 수정 ([5c83b3e](https://gitlab.com/monster-space-network/typemon/serverless/commit/5c83b3e28f6dfa2be6162c1d78a52447cbd5266f))



# [6.0.0-beta.21](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.20...6.0.0-beta.21) (2019-12-13)


### Features

* @types/aws-lambda 의존성 구성 변경 ([94d4db6](https://gitlab.com/monster-space-network/typemon/serverless/commit/94d4db69a017c75e0b921c4c6872314f1fb89c5c)), closes [#8](https://gitlab.com/monster-space-network/typemon/serverless/issues/8)



# [6.0.0-beta.20](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.19...6.0.0-beta.20) (2019-12-13)


### Features

* @typemon/scope 의존성 추가 ([cfbf7b9](https://gitlab.com/monster-space-network/typemon/serverless/commit/cfbf7b9d3a9ac4231db590d9b8dfc0a8b38a3968))
* metadata-key 수출 ([567b992](https://gitlab.com/monster-space-network/typemon/serverless/commit/567b99205c5f05a661ef911e3a92430145ec65a3))
* 공유 컨텍스트 기능 변경 및 리팩터링 ([1fab103](https://gitlab.com/monster-space-network/typemon/serverless/commit/1fab1038aa6033c09678efba52a0862df123afe0))
* 범위가 지정된 수명 지원 추가 ([94a9f81](https://gitlab.com/monster-space-network/typemon/serverless/commit/94a9f814e1defc03719b150022d73b9a9e5df305))
* **http-parameters:** 읽기 전용 유형에 iterable 유형 추가 ([d07f433](https://gitlab.com/monster-space-network/typemon/serverless/commit/d07f433c49df080d07dbd583b1c4e79aa8440a80))
* @typemon/pipeline 의존성 추가 ([8180445](https://gitlab.com/monster-space-network/typemon/serverless/commit/8180445cfb51c4670d37f1ca1c2a9d655a3dd84f))
* 의존성 변경 사항 반영 및 리팩터링 ([b34be15](https://gitlab.com/monster-space-network/typemon/serverless/commit/b34be158e893beabbdf79369fc05b08d8ad9d50b))
* 의존성 업데이트 ([0aa84ce](https://gitlab.com/monster-space-network/typemon/serverless/commit/0aa84ceda48b0faa25d07eece35f8dbeee19bacb))
* 팩토리 패턴 삭제 ([cf93f80](https://gitlab.com/monster-space-network/typemon/serverless/commit/cf93f80516fe3b69642cd54831582a3b21deca6e))
* **http-response:** status-code 범위 변경 ([dd02bba](https://gitlab.com/monster-space-network/typemon/serverless/commit/dd02bba003d55bd5be3ad21627a7b81f3083dc69))



# [6.0.0-beta.19](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.18...6.0.0-beta.19) (2019-11-08)


### Features

* 파이프 기능 추가 ([9ba28cf](https://gitlab.com/monster-space-network/typemon/serverless/commit/9ba28cf746b760a92c71b1a85cce8b1d9cb8bcbf))
* **pipeline:** 유효성 검사 추가 ([5935cf0](https://gitlab.com/monster-space-network/typemon/serverless/commit/5935cf06cce7d158f3947338993f4546db291878))
* **runner:** 파라미터 처리 방식 변경 ([d74975b](https://gitlab.com/monster-space-network/typemon/serverless/commit/d74975b95518e0d502ddb7ea2e7756cbeddb5823))



# [6.0.0-beta.18](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.17...6.0.0-beta.18) (2019-11-08)


### Features

* http 응답 처리 방식 변경 ([8342148](https://gitlab.com/monster-space-network/typemon/serverless/commit/834214876204353b4ea190d2f5f69511fe132973)), closes [#5](https://gitlab.com/monster-space-network/typemon/serverless/issues/5)



# [6.0.0-beta.17](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.16...6.0.0-beta.17) (2019-11-07)


### Features

* 의존성 업데이트 ([9825b89](https://gitlab.com/monster-space-network/typemon/serverless/commit/9825b891607a01bb413c8584c8554fa94e47ea1d))



# [6.0.0-beta.16](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.15...6.0.0-beta.16) (2019-10-22)


### Bug Fixes

* **runner:** 미들웨어 핸들러 파라미터 메타데이터 가져오기 문제 해결 ([0b64594](https://gitlab.com/monster-space-network/typemon/serverless/commit/0b6459461205d7e968a55d49a0fd6f5f945dc469))


### Features

* **http-runner:** body 직렬화 기능 변경 ([4f1189d](https://gitlab.com/monster-space-network/typemon/serverless/commit/4f1189ddb84a5cbe2519afefc498d0082f7204ed))
* **runner:** 미들웨어 next 함수 오류 메시지 변경 ([3ee08ed](https://gitlab.com/monster-space-network/typemon/serverless/commit/3ee08ed36e8e29e9a1117ad39649c370993f8a2d))



# [6.0.0-beta.15](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.14...6.0.0-beta.15) (2019-10-22)


### Bug Fixes

* **runner:** 파라미터 메타데이터 가져오기 문제 해결 ([cebe571](https://gitlab.com/monster-space-network/typemon/serverless/commit/cebe571a2cab206a1a774fc523790e1b72e1d6eb))


### Features

* 의존성 업데이트 ([e14b062](https://gitlab.com/monster-space-network/typemon/serverless/commit/e14b062e9d1f6df38e7a8ad3383945efb85a95b6))



# [6.0.0-beta.14](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.13...6.0.0-beta.14) (2019-10-22)


### Features

* shared-context 기능 변경 ([0185b6d](https://gitlab.com/monster-space-network/typemon/serverless/commit/0185b6d2e1b25b39eca7ff0aa1b59f7d44b17652))



# [6.0.0-beta.13](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.12...6.0.0-beta.13) (2019-10-22)


### Features

* 의존성 변경 사항 반영 ([54be7af](https://gitlab.com/monster-space-network/typemon/serverless/commit/54be7afb9c826eecc65f0bf0031300733846f78c))
* 의존성 업데이트 및 일부 의존성 삭제 ([1bcc636](https://gitlab.com/monster-space-network/typemon/serverless/commit/1bcc636b554701704f3822d432346fd5ea06a7ab))
* **controller:** metadata 주입 토큰, 데코레이터 추가 ([0727589](https://gitlab.com/monster-space-network/typemon/serverless/commit/07275893f9fd44c4aa099d5fafa856e89fe97b5a))
* **handler:** metadata 주입 토큰, 데코레이터 추가 ([1343990](https://gitlab.com/monster-space-network/typemon/serverless/commit/13439900735b106eabf8c478e06e543c94e5dc28))
* **runner:** controller, handler 메타데이터 파라미터 추가 ([bda4be1](https://gitlab.com/monster-space-network/typemon/serverless/commit/bda4be186c08b49645287b44968bddb3531ae5a4))



# [6.0.0-beta.12](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.11...6.0.0-beta.12) (2019-10-09)


### Features

* **handler:** providers 옵션 삭제 ([3b2cc2d](https://gitlab.com/monster-space-network/typemon/serverless/commit/3b2cc2d))
* **runner:** 구조 변경 및 리팩터링 ([ab61c63](https://gitlab.com/monster-space-network/typemon/serverless/commit/ab61c63))



# [6.0.0-beta.11](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.10...6.0.0-beta.11) (2019-10-04)


### Features

* **runner:** 컨테이너 구조 변경 ([b2175f4](https://gitlab.com/monster-space-network/typemon/serverless/commit/b2175f4))



# [6.0.0-beta.10](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.9...6.0.0-beta.10) (2019-09-30)


### Features

* **http-runner:** 결과 처리 기능 변경 ([b5de735](https://gitlab.com/monster-space-network/typemon/serverless/commit/b5de735))



# [6.0.0-beta.9](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.8...6.0.0-beta.9) (2019-09-27)


### Features

* **http-runner:** 에러 처리 기능 변경 ([597b6d2](https://gitlab.com/monster-space-network/typemon/serverless/commit/597b6d2))



# [6.0.0-beta.8](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.7...6.0.0-beta.8) (2019-09-27)


### Features

* 글로벌 옵션 추가 ([d07e588](https://gitlab.com/monster-space-network/typemon/serverless/commit/d07e588))



# [6.0.0-beta.7](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.6...6.0.0-beta.7) (2019-09-26)


### Bug Fixes

* **http-runner:** run 비동기 문제 해결 ([0f0034d](https://gitlab.com/monster-space-network/typemon/serverless/commit/0f0034d))



# [6.0.0-beta.6](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.5...6.0.0-beta.6) (2019-09-26)


### Bug Fixes

* **http-runner:** headers 파라미터 해결 기능 수정 ([f1343f3](https://gitlab.com/monster-space-network/typemon/serverless/commit/f1343f3))



# [6.0.0-beta.5](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.4...6.0.0-beta.5) (2019-09-26)


### Bug Fixes

* **runner:** 컨트롤러 제공자 바인드 누락 수정 ([c928577](https://gitlab.com/monster-space-network/typemon/serverless/commit/c928577))



# [6.0.0-beta.4](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.3...6.0.0-beta.4) (2019-09-26)


### Features

* **handler:** key 옵션 추가 ([d808f8b](https://gitlab.com/monster-space-network/typemon/serverless/commit/d808f8b))
* **serverless:** 사용자 지정 키 지원 기능 추가 ([541457b](https://gitlab.com/monster-space-network/typemon/serverless/commit/541457b))



# [6.0.0-beta.3](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.2...6.0.0-beta.3) (2019-09-26)


### Features

* 의존성 업데이트 ([3f64c93](https://gitlab.com/monster-space-network/typemon/serverless/commit/3f64c93))



# [6.0.0-beta.2](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.1...6.0.0-beta.2) (2019-09-26)


### Features

* 의존성 구성 변경 ([fdb30be](https://gitlab.com/monster-space-network/typemon/serverless/commit/fdb30be))



# [6.0.0-beta.1](https://gitlab.com/monster-space-network/typemon/serverless/compare/6.0.0-beta.0...6.0.0-beta.1) (2019-09-25)


### Features

* 의존성 추가 ([f906405](https://gitlab.com/monster-space-network/typemon/serverless/commit/f906405))
* **http-runner:** boom 지원 추가 및 예외 처리 기능 추가 ([da94ffa](https://gitlab.com/monster-space-network/typemon/serverless/commit/da94ffa))
* **runner:** 예외 처리 기능 삭제 ([75bbf1c](https://gitlab.com/monster-space-network/typemon/serverless/commit/75bbf1c))
