# Serverless - [![version](https://img.shields.io/npm/v/@typemon/serverless.svg)](https://www.npmjs.com/package/@typemon/serverless) [![license](https://img.shields.io/npm/l/@typemon/serverless.svg)](https://gitlab.com/monster-space-network/typemon/serverless/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/serverless/dev/typescript.svg)
- Easier function definition with decorators
- Simplified dependency management with dependency injection
- Powerful middleware, pipe
- Abstracted http event objects
- [`@hapi/boom`](https://github.com/hapijs/boom) support


## Installation
```
$ npm install @typemon/serverless
```
- Install event definitions if necessary.
    - https://www.npmjs.com/package/@types/aws-lambda
```
$ npm install -D @types/aws-lambda
```

#### [TypeScript](https://github.com/Microsoft/TypeScript)
- Version **`3.5 or later`** is required.
- Configure the decorator and metadata options.
    - https://www.typescriptlang.org/docs/handbook/decorators.html#metadata
```json
{
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
}
```



## Usage
See [example](https://gitlab.com/monster-space-network/typemon/serverless/tree/master/example).
