import { Metadata } from '@typemon/reflection';
import { InjectionToken, Injectable, Inject } from '@typemon/dependency-injection';
import { Pipeable } from '@typemon/pipeline';
//
import { MetadataKey } from './metadata-key';
import { HandlerDecorator } from './handler-decorator';
import { HandlerParameterDecorator } from './handler-parameter-decorator';
import { Middleware } from './middleware';
//
//
//
export function Handler(options: Handler.Options = {}): MethodDecorator {
    return HandlerDecorator.create((metadata: Metadata): void => {
        if (metadata.has(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        const target: Object = metadata.owner.target;
        const propertyKey: string | symbol = metadata.owner.propertyKey as string | symbol;

        Injectable.decorate(metadata.owner);
        metadata.set(MetadataKey.OPTIONS, options);

        const propertyKeys: ReadonlyArray<string | symbol> = Metadata.has(MetadataKey.HANDLER_PROPERTY_KEYS, target.constructor)
            ? Metadata.get(MetadataKey.HANDLER_PROPERTY_KEYS, target.constructor)
            : [];

        Metadata.set(MetadataKey.HANDLER_PROPERTY_KEYS, propertyKeys.concat(propertyKey), target.constructor);
    });
}
export namespace Handler {
    export interface Options {
        /**
         * @default propertyKey
         */
        readonly key?: string;

        /**
         * @default true
         */
        readonly http?: boolean;

        readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
    }

    export const METADATA_TOKEN: InjectionToken = new InjectionToken('typemon.serverless.handler.metadata');
    export function Metadata(): HandlerParameterDecorator {
        return HandlerParameterDecorator.create((metadata: Metadata): void => {
            Inject.decorate(METADATA_TOKEN, metadata.owner);
            Pipeable.decorate(false, metadata.owner);
        });
    }
}
