import { Metadata } from '@typemon/reflection';
import { Provider, Injectable, InjectionToken, Inject } from '@typemon/dependency-injection';
import { Pipeable } from '@typemon/pipeline';
//
import { MetadataKey } from './metadata-key';
import { HandlerParameterDecorator } from './handler-parameter-decorator';
//
//
//
export interface Middleware {
    handler(...parameters: ReadonlyArray<any>): void | Promise<void>;
}
export function Middleware(options: Middleware.Options = {}): ClassDecorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        if (metadata.has(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        Injectable.decorate(metadata.owner);
        metadata.set(MetadataKey.OPTIONS, options);
    });
}
export namespace Middleware {
    export type Constructor = new (...parameters: ReadonlyArray<any>) => Middleware;

    export interface Options {
        readonly providers?: ReadonlyArray<Provider>;
    }
}

export type Next = () => void;
export function Next(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata) => {
        Inject.decorate(Next.TOKEN, metadata.owner);
        Pipeable.decorate(false, metadata.owner);
    });
}
export namespace Next {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverless.middleware.next');
}
