import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
//
//
//
export type HandlerDecorator = MethodDecorator;
export namespace HandlerDecorator {
    export type Handler = Metadata.Decorator.Handler;

    export function create(handler: Handler): HandlerDecorator {
        return Metadata.Decorator.create((metadata: Metadata): void => {
            if (Check.isFunction(metadata.owner.target) || Check.isNotUndefined(metadata.owner.parameterIndex)) {
                throw new Error('This decorator can only be used for instance methods.');
            }

            if (Check.isSymbol(metadata.owner.propertyKey)) {
                throw new Error('Invalid property key.');
            }

            handler(metadata);
        });
    }
}
