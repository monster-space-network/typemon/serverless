import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Injectable, Inject } from '@typemon/dependency-injection';
//
import { HandlerParameterDecorator } from './handler-parameter-decorator';
//
//
//
@Injectable()
export class ServerlessScopedContext {
    private readonly map: Map<string, unknown>;

    public constructor() {
        this.map = new Map();
    }

    public has(key: string): boolean {
        return this.map.has(key);
    }
    public hasNot(key: string): boolean {
        return Check.isFalse(this.has(key));
    }

    public get(key: string): any {
        if (this.hasNot(key)) {
            throw new Error('Key does not exist.');
        }

        return this.map.get(key);
    }

    public set(key: string, value: unknown): void {
        this.map.set(key, value);
    }

    public delete(key: string): void {
        this.map.delete(key);
    }

    public clear(): void {
        this.map.clear();
    }
}

export type ScopedContext = ServerlessScopedContext;
export function ScopedContext(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(ServerlessScopedContext, metadata.owner));
}
