//
//
//
export namespace MetadataKey {
    export const OPTIONS: unique symbol = Symbol('typemon.serverless.options');

    export const HANDLER_PROPERTY_KEYS: unique  symbol = Symbol('typemon.serverless.handler-property-keys');

    export const KEY: unique symbol = Symbol('typemon.serverless.typemon.key');
    export const NAME: unique symbol = Symbol('typemon.serverless.typemon.name');

    export namespace Response {
        export const STATUS_CODE: unique symbol = Symbol('typemon.serverless.http.response.status-code');
        export const HEADERS: unique symbol = Symbol('typemon.serverless.http.response.headers');
    }
}
