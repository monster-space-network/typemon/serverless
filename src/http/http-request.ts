import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Inject, InjectionToken } from '@typemon/dependency-injection';
import { APIGatewayProxyEvent } from 'aws-lambda';
//
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
import { HttpHeaders, ReadonlyHttpHeaders } from './http-headers';
import { ReadonlyHttpParameters, HttpParameters } from './http-parameters';
//
//
//
export class HttpRequest<Body extends object = any> {
    public readonly path: string;
    public readonly method: string;

    public readonly headers: ReadonlyHttpHeaders;
    public readonly pathParameters: ReadonlyHttpParameters;
    public readonly queryStringParameters: ReadonlyHttpParameters;

    public readonly body: Body;

    public constructor(event: APIGatewayProxyEvent) {
        this.path = event.path;
        this.method = event.requestContext.httpMethod;

        this.headers = new HttpHeaders(event.headers);
        this.pathParameters = new HttpParameters(event.pathParameters ?? {});
        this.queryStringParameters = new HttpParameters(event.queryStringParameters ?? {});

        this.body = Check.isNotNull(event.body)
            ? JSON.parse(event.body)
            : {};
    }
}

export type Request = HttpRequest;
export function Request(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(HttpRequest, metadata.owner));
}

export function Body(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(Body.TOKEN, metadata.owner));
}
export namespace Body {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverless.http.request.body');
}
