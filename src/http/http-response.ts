import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Inject } from '@typemon/dependency-injection';
//
import { HandlerDecorator } from '../handler-decorator';
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
import { HttpHeaders } from './http-headers';
import { MetadataKey } from '../metadata-key';
//
//
//
export class HttpResponse {
    public statusCode: number;
    public readonly headers: HttpHeaders;
    public body: any;

    public constructor() {
        this.statusCode = 200;
        this.headers = new HttpHeaders();
        this.body = null;
    }
}

export type Response = HttpResponse;
export function Response(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(HttpResponse, metadata.owner));
}
export namespace Response {
    export function StatusCode(statusCode: number): MethodDecorator {
        if (Check.isNotInteger(statusCode) || Check.less(statusCode, 200) || Check.greater(statusCode, 511)) {
            throw new Error('Invalid status code.');
        }

        return HandlerDecorator.create((metadata: Metadata): void => {
            if (metadata.has(MetadataKey.Response.STATUS_CODE)) {
                throw new Error('This decorator cannot be used in duplicate.');
            }

            metadata.set(MetadataKey.Response.STATUS_CODE, statusCode);
        });
    }

    export function Headers(headers: Readonly<Record<string, string>>): MethodDecorator {
        return HandlerDecorator.create((metadata: Metadata): void => {
            if (metadata.has(MetadataKey.Response.HEADERS)) {
                throw new Error('This decorator cannot be used in duplicate.');
            }

            metadata.set(MetadataKey.Response.HEADERS, headers);
        });
    }
}
