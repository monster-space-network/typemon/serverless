import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Inject } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../metadata-key';
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
import { HttpParameters, ReadonlyHttpParameters } from './http-parameters';
//
//
//
export class HttpHeaders extends HttpParameters {
    private getKey(name: string): string {
        name = name.toLowerCase();

        for (const key of this.map.keys()) {
            if (Check.equal(key, name)) {
                return key;
            }
        }

        return name;
    }

    public has(name: string): boolean {
        return this.map.has(this.getKey(name));
    }
    public hasNot(name: string): boolean {
        return Check.isFalse(this.has(name));
    }

    public get(name: string): any {
        if (this.hasNot(name)) {
            throw new Error('Name does not exist.');
        }

        return this.map.get(this.getKey(name));
    }

    public set(name: string, value: string): void {
        this.map.set(this.getKey(name), value);
    }

    public delete(name: string): void {
        this.map.delete(this.getKey(name));
    }
}
export type ReadonlyHttpHeaders = ReadonlyHttpParameters;

export type Headers = ReadonlyHttpHeaders;
export function Headers(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(HttpHeaders, metadata.owner));
}

export function Header(name: string): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => {
        Inject.decorate(HttpHeaders, metadata.owner);
        metadata.set(MetadataKey.NAME, name);
    });
}
