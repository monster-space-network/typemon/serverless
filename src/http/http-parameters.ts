import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Inject, InjectionToken } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../metadata-key';
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
//
//
//
export class HttpParameters implements Iterable<[string, string]> {
    protected readonly map: Map<string, string>;

    public constructor(initialParameters: object = []) {
        this.map = new Map();
        this.initialize(initialParameters);
    }

    private initialize(initialParameters: object): void {
        const entries: Iterable<[string, string]> = Check.isNotIterable(initialParameters)
            ? Object.entries(initialParameters)
            : initialParameters;

        for (const [key, value] of entries) {
            this.set(key, value);
        }
    }

    public [Symbol.iterator](): Iterator<[string, string]> {
        return this.map[Symbol.iterator]();
    }

    public has(key: string): boolean {
        return this.map.has(key);
    }
    public hasNot(key: string): boolean {
        return Check.isFalse(this.has(key));
    }

    public get(key: string): any {
        if (this.hasNot(key)) {
            throw new Error('Key does not exist.');
        }

        return this.map.get(key);
    }

    public set(key: string, value: string): void {
        this.map.set(key, value);
    }

    public delete(key: string): void {
        this.map.delete(key);
    }

    public clear(): void {
        this.map.clear();
    }
}
export type ReadonlyHttpParameters = Omit<HttpParameters, 'set' | 'delete' | 'clear'> & Iterable<[string, string]>;

export type PathParameters = ReadonlyHttpParameters;
export function PathParameters(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(PathParameters.TOKEN, metadata.owner));
}
export namespace PathParameters {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverless.http.request.path-parameters');
}

export function PathParameter(key: string): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => {
        Inject.decorate(PathParameters.TOKEN, metadata.owner);
        metadata.set(MetadataKey.KEY, key);
    });
}

export type QueryStringParameters = ReadonlyHttpParameters;
export function QueryStringParameters(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(QueryStringParameters.TOKEN, metadata.owner));
}
export namespace QueryStringParameters {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverless.http.request.query-string-parameters');
}

export function QueryStringParameter(key: string): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => {
        Inject.decorate(QueryStringParameters.TOKEN, metadata.owner);
        metadata.set(MetadataKey.KEY, key);
    });
}
