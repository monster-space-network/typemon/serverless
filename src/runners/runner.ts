import { Check } from '@typemon/check';
import { Metadata, Reflection } from '@typemon/reflection';
import { Scope } from '@typemon/scope';
import { Constructor, Container, Lifetime, Identifier, MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
import { Pipeline } from '@typemon/pipeline';
import Lambda from 'aws-lambda';
//
import { MetadataKey } from '../metadata-key';
import { ServerlessScopedContext } from '../scoped-context';
import { Event, Context } from '../lambda';
import { Middleware, Next } from '../middleware';
import { Controller } from '../controller';
import { Handler } from '../handler';
import { GlobalOptions } from '../global-options';
//
//
//
export class Runner {
    protected readonly target: Constructor;
    protected readonly propertyKey: string;
    protected readonly globalOptions: GlobalOptions;

    protected readonly controllerMetadata: Metadata;
    protected readonly controllerOptions: Controller.Options;

    protected readonly handlerMetadata: Metadata;
    protected readonly handlerOptions: Handler.Options;

    protected readonly dependenciesContainer: Container;
    protected readonly parametersContainer: Container;

    public constructor({ event, context, target, propertyKey, globalOptions }: Runner.Context) {
        this.target = target;
        this.propertyKey = propertyKey;
        this.globalOptions = globalOptions;

        this.controllerMetadata = Metadata.of(this.target);
        this.controllerOptions = this.controllerMetadata.get(MetadataKey.OPTIONS);

        this.handlerMetadata = Metadata.of(this.target.prototype, this.propertyKey);
        this.handlerOptions = this.handlerMetadata.get(MetadataKey.OPTIONS);

        this.dependenciesContainer = new Container();
        this.parametersContainer = new Container({
            defaultLifetime: Lifetime.Singleton
        });

        this.dependenciesContainer.bindAll(this.globalOptions.providers ?? []);
        this.dependenciesContainer.bindAll(this.controllerOptions.providers ?? []);
        this.dependenciesContainer.bind({
            identifier: Controller.METADATA_TOKEN,
            useValue: this.controllerMetadata
        });
        this.dependenciesContainer.bind({
            identifier: Handler.METADATA_TOKEN,
            useValue: this.handlerMetadata
        });

        this.parametersContainer.bind(ServerlessScopedContext);
        this.parametersContainer.bind({
            identifier: Event.TOKEN,
            useValue: event
        });
        this.parametersContainer.bind({
            identifier: Context.TOKEN,
            useValue: context
        });
        this.parametersContainer.bind({
            identifier: Controller.METADATA_TOKEN,
            useValue: this.controllerMetadata
        });
        this.parametersContainer.bind({
            identifier: Handler.METADATA_TOKEN,
            useValue: this.handlerMetadata
        });
    }

    private async execute(): Promise<unknown> {
        const middlewares: ReadonlyArray<Middleware.Constructor> = [
            ...(this.globalOptions.middlewares ?? []),
            ...(this.controllerOptions.middlewares ?? []),
            ...(this.handlerOptions.middlewares ?? [])
        ];
        const stop: boolean = await this.executeMiddlewares(middlewares);

        if (stop) {
            return this.generateResult(false);
        }

        const output: unknown = await this.executeHandler();

        return this.generateResult(true, output);
    }

    private async resolveParameter(parameterMetadata: Metadata): Promise<unknown> {
        const identifier: Identifier = parameterMetadata.get(DIMetadataKey.IDENTIFIER);
        const parameter: unknown = await this.getParameter(identifier, parameterMetadata);
        const pipedParameter: unknown = await Pipeline.resolve(parameter, parameterMetadata, this.dependenciesContainer);

        return pipedParameter;
    }
    private async resolveParameters(parametersMetadata: ReadonlyArray<Metadata>, callback?: (parameterMetadata: Metadata) => Promise<unknown>): Promise<ReadonlyArray<unknown>> {
        const parameters: Array<unknown> = [];

        for (const parameterMetadata of parametersMetadata) {
            const parameterResolutionPromise: Promise<unknown> = Check.isUndefined(callback)
                ? this.resolveParameter(parameterMetadata)
                : callback(parameterMetadata);
            const parameter: unknown = await parameterResolutionPromise;

            parameters.push(parameter);
        }

        return parameters;
    }

    private async executeMiddleware(target: Middleware.Constructor): Promise<boolean> {
        const options: Middleware.Options = Metadata.get(MetadataKey.OPTIONS, target);
        const container: Container = new Container({
            parent: this.dependenciesContainer
        });
        let stop: boolean = true;

        container.bindAll(options.providers ?? []);

        const { length: parametersLength }: ReadonlyArray<unknown> = Reflection.getMetadata(DIMetadataKey.DESIGN_PARAM_TYPES, target.prototype, 'handler');
        const parametersMetadata: ReadonlyArray<Metadata> = new Array(parametersLength).fill(null).map((_: null, index: number): Metadata => Metadata.of(target.prototype, 'handler', index));
        const parameters: ReadonlyArray<unknown> = await this.resolveParameters(parametersMetadata, async (parameterMetadata: Metadata): Promise<unknown> => {
            const identifier: Identifier = parameterMetadata.has(DIMetadataKey.IDENTIFIER)
                ? parameterMetadata.get(DIMetadataKey.IDENTIFIER)
                : parameterMetadata.get(DIMetadataKey.TYPE);

            switch (identifier) {
                case Next.TOKEN:
                    return (): void => {
                        if (Check.isFalse(stop)) {
                            throw new Error('Do not call the next function more than once.');
                        }

                        stop = false;
                    };

                default:
                    return this.resolveParameter(parameterMetadata);
            }
        });
        const instance: Middleware = await container.resolve(target);

        await instance.handler(...parameters);

        return stop;
    }
    private async executeMiddlewares(targets: Iterable<Middleware.Constructor>): Promise<boolean> {
        for (const target of targets) {
            const stop: boolean = await this.executeMiddleware(target);

            if (stop) {
                return true;
            }
        }

        return false;
    }

    private async executeHandler(): Promise<unknown> {
        const parametersLength: number = this.handlerMetadata.get(DIMetadataKey.PARAMETERS_LENGTH);
        const parametersMetadata: ReadonlyArray<Metadata> = new Array(parametersLength).fill(null).map((_: null, index: number): Metadata => this.handlerMetadata.of(index));
        const parameters: ReadonlyArray<unknown> = await this.resolveParameters(parametersMetadata);
        const instance: any = await this.dependenciesContainer.resolve(this.target);
        const output: unknown = await instance[this.propertyKey](...parameters);

        return output;
    }

    protected getParameter(identifier: Identifier, _: Metadata): Promise<unknown> {
        return this.parametersContainer.get(identifier);
    }

    protected async generateResult(_: boolean, output?: any): Promise<unknown> {
        return output;
    }

    public run(): Promise<unknown> {
        return Scope.run((): Promise<unknown> => this.execute());
    }
}
export namespace Runner {
    export interface Context {
        readonly event: any;
        readonly context: Lambda.Context;
        readonly target: Constructor;
        readonly propertyKey: string;
        readonly globalOptions: GlobalOptions;
    }
}
