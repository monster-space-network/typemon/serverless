import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Identifier } from '@typemon/dependency-injection';
import Boom from '@hapi/boom';
//
import { Event } from '../lambda';
import {
    HttpRequest, HttpResponse,
    HttpHeaders, ReadonlyHttpHeaders,
    PathParameters, QueryStringParameters, ReadonlyHttpParameters,
    Body
} from '../http';
import { Runner } from './runner';
import { MetadataKey } from '../metadata-key';
//
//
//
interface SerializedResponse {
    readonly statusCode: number;
    readonly headers: Readonly<Record<string, string>>;
    readonly body: string;
}

function serializeResponse(response: HttpResponse): SerializedResponse {
    return {
        statusCode: response.statusCode,
        headers: Array
            .from(response.headers)
            .reduce((headers: object, [name, value]: [string, string]): object => ({
                ...headers,
                [name]: value
            }), {}),
        body: Check.isNotUndefinedAndNotNull(response.body)
            ? Check.isNotString(response.body)
                ? JSON.stringify(response.body)
                : response.body
            : ''
    };
}

export class HttpRunner extends Runner {
    private async handleBoom(boom: Boom.Boom): Promise<SerializedResponse> {
        const response: HttpResponse = await this.parametersContainer.get(HttpResponse);
        const { output, data }: Boom.Boom = boom;

        response.statusCode = output.statusCode;
        response.body = {
            statusCode: output.statusCode,
            error: output.payload.error,
            message: output.payload.message,
            data: data ?? null
        };

        return serializeResponse(response);
    }

    private resolveHttpParameters(parameters: ReadonlyHttpParameters, metadata: Metadata): unknown {
        const key: null | string = metadata.has(MetadataKey.KEY)
            ? metadata.get(MetadataKey.KEY)
            : null;

        if (Check.isNull(key)) {
            return parameters;
        }

        if (parameters.hasNot(key)) {
            return null;
        }

        return parameters.get(key);
    }

    protected async getParameter(identifier: Identifier, metadata: Metadata): Promise<unknown> {
        switch (identifier) {
            case HttpHeaders: {
                const request: HttpRequest = await this.parametersContainer.get(HttpRequest);
                const headers: ReadonlyHttpHeaders = request.headers;
                const name: null | string = metadata.has(MetadataKey.NAME)
                    ? metadata.get(MetadataKey.NAME)
                    : null;

                if (Check.isNull(name)) {
                    return headers;
                }

                if (headers.hasNot(name)) {
                    return null;
                }

                return headers.get(name);
            }

            case PathParameters.TOKEN: {
                const request: HttpRequest = await this.parametersContainer.get(HttpRequest);

                return this.resolveHttpParameters(request.pathParameters, metadata);
            }

            case QueryStringParameters.TOKEN: {
                const request: HttpRequest = await this.parametersContainer.get(HttpRequest);

                return this.resolveHttpParameters(request.queryStringParameters, metadata);
            }

            case Body.TOKEN: {
                const request: HttpRequest = await this.parametersContainer.get(HttpRequest);

                return request.body;
            }

            default:
                return super.getParameter(identifier, metadata);
        }
    }

    protected async generateResult(handlerExecuted: boolean, output?: unknown): Promise<SerializedResponse> {
        const response: HttpResponse = await this.parametersContainer.get(HttpResponse);

        if (handlerExecuted) {
            if (this.handlerMetadata.has(MetadataKey.Response.STATUS_CODE)) {
                response.statusCode = this.handlerMetadata.get(MetadataKey.Response.STATUS_CODE);
            }

            if (this.handlerMetadata.has(MetadataKey.Response.HEADERS)) {
                const headers: Readonly<Record<string, string>> = this.handlerMetadata.get(MetadataKey.Response.HEADERS);
                const entries: Iterable<[string, string]> = Object.entries(headers);

                for (const [name, value] of entries) {
                    response.headers.set(name, value);
                }
            }
        }

        return serializeResponse({
            statusCode: response.statusCode,
            headers: response.headers,
            body: output ?? response.body
        });
    }

    public async run(): Promise<unknown> {
        this.parametersContainer.bind({
            identifier: HttpRequest,
            useFactory: (event: any): HttpRequest => new HttpRequest(event),
            dependencies: [Event.TOKEN]
        });
        this.parametersContainer.bind({
            identifier: HttpResponse,
            useValue: new HttpResponse()
        });

        try {
            return await super.run();
        }
        catch (error) {
            if (Boom.isBoom(error)) {
                return this.handleBoom(error);
            }

            // tslint:disable-next-line: no-console
            console.error(error);

            return this.handleBoom(Boom.internal());
        }
    }
}
