import { Check } from '@typemon/check';
import { Metadata } from '@typemon/reflection';
import { Constructor } from '@typemon/dependency-injection';
//
import { MetadataKey } from './metadata-key';
import { Context } from './lambda';
import { Handler } from './handler';
import { Controller } from './controller';
import { GlobalOptions } from './global-options';
import { Runner, HttpRunner } from './runners';
//
//
//
export namespace Serverless {
    export type EntryPoint = (event: unknown, context: Context) => Promise<unknown>;
    export type EntryPoints = Readonly<Record<string, EntryPoint>>;

    let globalOptions: GlobalOptions = {};

    export function globally(options: GlobalOptions): void {
        globalOptions = options;
    }

    export function build(target: Constructor): EntryPoints {
        const controllerMetadata: Metadata = Metadata.of(target);

        if (controllerMetadata.hasNot(MetadataKey.OPTIONS) || controllerMetadata.hasNot(MetadataKey.HANDLER_PROPERTY_KEYS)) {
            throw new Error('The target is not a controller.');
        }

        const controllerOptions: Controller.Options = controllerMetadata.get(MetadataKey.OPTIONS);
        const handlerPropertyKeys: ReadonlyArray<string> = controllerMetadata.get(MetadataKey.HANDLER_PROPERTY_KEYS);
        const entryPoints: EntryPoints = handlerPropertyKeys.reduce((entryPoints: EntryPoints, propertyKey: string): EntryPoints => {
            const handlerMetadata: Metadata = Metadata.of(target.prototype, propertyKey);
            const handlerOptions: Handler.Options = handlerMetadata.get(MetadataKey.OPTIONS);
            const http: boolean = Check.isUndefined(handlerOptions.http)
                ? Check.isUndefined(controllerOptions.http)
                    ? Check.isNotFalse(globalOptions.http)
                    : Check.isNotFalse(controllerOptions.http)
                : Check.isNotFalse(handlerOptions.http);
            const key: string = handlerOptions.key ?? propertyKey;

            return {
                ...entryPoints,
                [key]: (event: unknown, context: Context): Promise<unknown> => {
                    const runnerContext: Runner.Context = {
                        event,
                        context,
                        target,
                        propertyKey,
                        globalOptions
                    };
                    const runner: Runner = http
                        ? new HttpRunner(runnerContext)
                        : new Runner(runnerContext);

                    return runner.run();
                }
            };
        }, {});

        return entryPoints;
    }
}
