import { Metadata } from '@typemon/reflection';
import { Inject, InjectionToken } from '@typemon/dependency-injection';
import Lambda from 'aws-lambda';
//
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
//
//
//
export type Context = Lambda.Context;
export function Context(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(Context.TOKEN, metadata.owner));
}
export namespace Context {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverless.context');
}
