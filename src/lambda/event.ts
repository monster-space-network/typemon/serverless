import { Metadata } from '@typemon/reflection';
import { Inject, InjectionToken } from '@typemon/dependency-injection';
//
import { HandlerParameterDecorator } from '../handler-parameter-decorator';
//
//
//
export function Event(): HandlerParameterDecorator {
    return HandlerParameterDecorator.create((metadata: Metadata): void => Inject.decorate(Event.TOKEN, metadata.owner));
}
export namespace Event {
    export const TOKEN: InjectionToken = new InjectionToken('typemon.serverless.event');
}
