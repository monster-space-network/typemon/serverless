import { Metadata } from '@typemon/reflection';
import { InjectionToken, Provider, Injectable, Inject } from '@typemon/dependency-injection';
import { Pipeable } from '@typemon/pipeline';
//
import { MetadataKey } from './metadata-key';
import { HandlerParameterDecorator } from './handler-parameter-decorator';
import { Middleware } from './middleware';
//
//
//
export function Controller(options: Controller.Options = {}): ClassDecorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        if (metadata.has(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        Injectable.decorate(metadata.owner);
        metadata.set(MetadataKey.OPTIONS, options);

        if (metadata.hasNot(MetadataKey.HANDLER_PROPERTY_KEYS)) {
            metadata.set(MetadataKey.HANDLER_PROPERTY_KEYS, []);
        }
    });
}
export namespace Controller {
    export interface Options {
        /**
         * @default true
         */
        readonly http?: boolean;

        readonly providers?: ReadonlyArray<Provider>;
        readonly middlewares?: ReadonlyArray<Middleware.Constructor>;
    }

    export const METADATA_TOKEN: InjectionToken = new InjectionToken('typemon.serverless.controller.metadata');
    export function Metadata(): HandlerParameterDecorator {
        return HandlerParameterDecorator.create((metadata: Metadata): void => {
            Inject.decorate(METADATA_TOKEN, metadata.owner);
            Pipeable.decorate(false, metadata.owner);
        });
    }
}
