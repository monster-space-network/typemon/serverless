import { Check } from '@typemon/check';
import { Configuration, NormalModuleReplacementPlugin } from 'webpack';
import Path from 'path';
import nodeExternals from 'webpack-node-externals';
import TSConfigPathsPlugin from 'tsconfig-paths-webpack-plugin';

const ServerlessWebpack: any = require('serverless-webpack');
//
//
//
const config: Configuration = {
    target: 'node',
    mode: 'development',
    devtool: 'inline-source-map',
    entry: ServerlessWebpack.lib.entries,
    module: {
        rules: [{
            test: /\.ts$/,
            loader: 'ts-loader'
        }]
    },
    resolve: {
        plugins: [new TSConfigPathsPlugin()],
        extensions: ['.ts']
    },
    externals: [nodeExternals()]
};

if (Check.isFalse(ServerlessWebpack.lib.webpack.isLocal)) {
    config.mode = 'production';
    config.plugins = [
        new NormalModuleReplacementPlugin(/variables\.ts/, Path.resolve('src', 'environments', 'variables.production.ts'))
    ];
}

export = config;
