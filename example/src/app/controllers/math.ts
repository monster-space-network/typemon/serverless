import { Pipeline } from '@typemon/pipeline';
import { Serverless, Controller, Handler, PathParameter } from '@typemon/serverless';
//
import '@globally';
import { ToIntegerPipe } from '@pipes';
//
//
//
@Controller()
class MathController {
    @Handler()
    public sum(
        @PathParameter('a') @Pipeline(ToIntegerPipe) a: number,
        @PathParameter('b') @Pipeline(ToIntegerPipe) b: number
    ): number {
        return a + b;
    }
}

export = Serverless.build(MathController);
