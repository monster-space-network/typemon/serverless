import { Serverless, Controller, Handler, Response } from '@typemon/serverless';
//
import '@globally';
import { MOTDService } from '@services';
//
//
//
@Controller()
class RootController {
    public constructor(
        private readonly motd: MOTDService
    ) { }

    @Handler()
    @Response.StatusCode(302)
    @Response.Headers({
        location: 'https://gitlab.com/monster-space-network/typemon/serverless'
    })
    public index(): void { }

    @Handler({
        key: 'get-motd'
    })
    public getMOTD(): Promise<string> {
        return this.motd.get();
    }
}

export = Serverless.build(RootController);
