import { Check } from '@typemon/check';
import { Pipe } from '@typemon/pipeline';
import Boom from '@hapi/boom';
//
//
//
@Pipe()
export class ToIntegerPipe implements Pipe {
    public apply(value: unknown): number {
        if (Check.isNotString(value)) {
            throw Boom.badRequest();
        }

        const parsedValue: number = Number.parseInt(value, 10);

        if (Check.isNaN(parsedValue)) {
            throw Boom.badRequest();
        }

        return parsedValue;
    }
}
