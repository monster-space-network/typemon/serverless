import { Lifetime } from '@typemon/dependency-injection';
import { Serverless } from '@typemon/serverless';
//
import '@config';
import { MOTDService } from '@services';
//
//
//
Serverless.globally({
    providers: [
        {
            identifier: MOTDService,
            useClass: MOTDService,
            lifetime: Lifetime.Singleton
        }
    ]
});
