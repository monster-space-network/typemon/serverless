//
//
//
export interface Constants {
    readonly aws: {
        readonly region: 'ap-northeast-2';
    };
}

export interface Variables {
    readonly production: boolean;
}

export type Environment = Constants & Variables;
