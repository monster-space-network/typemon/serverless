import { merge } from '@typemon/merge';
//
import { Environment } from './types';
import { constants } from './constants';
import { variables } from './variables';
//
//
//
export const environment: Environment = merge([
    constants,
    variables
]);
