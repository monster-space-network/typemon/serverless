# Serverless Example
> Preconfigured project to get started immediately.



## Installation
```
$ npm install
```

## Start Offline
```
$ npm start
```

## Deploy
```
$ npm run deploy
```
