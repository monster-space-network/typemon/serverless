import { Metadata } from '@typemon/reflection';
import { InjectionToken, MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../src/metadata-key';
import { Middleware, Next } from '../src';
//
//
//
test('default', () => {
    const options: Middleware.Options = {
        providers: [{
            identifier: new InjectionToken('foo'),
            useValue: 'bar'
        }]
    };

    @Middleware(options)
    class Target {
        public constructor(
            constructorParameter1: string,
            constructorParameter2: number
        ) { }
    }

    expect(Metadata.get(MetadataKey.OPTIONS, Target)).toMatchObject(options);
    expect(Metadata.get(DIMetadataKey.TYPE, Target, 0)).toBe(String);
    expect(Metadata.get(DIMetadataKey.TYPE, Target, 1)).toBe(Number);
});

test('duplicate', () => {
    expect(() => {
        @Middleware()
        @Middleware()
        class Target { }
    }).toThrow('This decorator cannot be used in duplicate.');
});

test('next', () => {
    class Target {
        public instanceMethod(
            @Next() instanceMethodParameter: Next
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Next.TOKEN);
});
