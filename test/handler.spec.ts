import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../src/metadata-key';
import { HttpRequest } from '../src/http';
import { Handler, Middleware } from '../src';
//
//
//
test('single', () => {
    const options: Handler.Options = {
        http: false,
        middlewares: [class B implements Middleware { public handler(): void { } }]
    };

    class Target {
        @Handler(options)
        public instanceMethod(
            instanceMethodParameter1: string,
            instanceMethodParameter2: number
        ): void { }
    }

    expect(Metadata.get(MetadataKey.HANDLER_PROPERTY_KEYS, Target)).toContain('instanceMethod');
    expect(Metadata.get(MetadataKey.OPTIONS, Target.prototype, 'instanceMethod')).toMatchObject(options);
    expect(Metadata.get(DIMetadataKey.TYPE, Target.prototype, 'instanceMethod', 0)).toBe(String);
    expect(Metadata.get(DIMetadataKey.TYPE, Target.prototype, 'instanceMethod', 1)).toBe(Number);
});

test('multiple', () => {
    const options: Handler.Options = {
        http: false,
        middlewares: [class B implements Middleware { public handler(): void { } }]
    };

    class Target {
        @Handler()
        public instanceMethod1(
            instanceMethodParameter1: string,
            instanceMethodParameter2: number
        ): void { }

        @Handler(options)
        public instanceMethod2(
            instanceMethodParameter1: boolean,
            instanceMethodParameter2: HttpRequest
        ): void { }
    }

    expect(Metadata.get(MetadataKey.HANDLER_PROPERTY_KEYS, Target)).toEqual(expect.arrayContaining(['instanceMethod1', 'instanceMethod2']));

    expect(Metadata.get(MetadataKey.OPTIONS, Target.prototype, 'instanceMethod1')).toMatchObject({});
    expect(Metadata.get(DIMetadataKey.TYPE, Target.prototype, 'instanceMethod1', 0)).toBe(String);
    expect(Metadata.get(DIMetadataKey.TYPE, Target.prototype, 'instanceMethod1', 1)).toBe(Number);

    expect(Metadata.get(MetadataKey.OPTIONS, Target.prototype, 'instanceMethod2')).toMatchObject(options);
    expect(Metadata.get(DIMetadataKey.TYPE, Target.prototype, 'instanceMethod2', 0)).toBe(Boolean);
    expect(Metadata.get(DIMetadataKey.TYPE, Target.prototype, 'instanceMethod2', 1)).toBe(HttpRequest);
});

test('duplicate', () => {
    expect(() => {
        class Target {
            @Handler()
            @Handler()
            public instanceMethod(): void { }
        }
    }).toThrow('This decorator cannot be used in duplicate.');
});

test('metadata', () => {
    class Target {
        public handler(
            @Handler.Metadata() metadata: Metadata
        ): void { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'handler', 0)).toBe(Handler.METADATA_TOKEN);
});
