import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../../src/metadata-key';
import { PathParameters } from '../../src/http';
import { PathParameter } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @PathParameter('test') instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(PathParameters.TOKEN);
    expect(Metadata.get(MetadataKey.KEY, Target.prototype, 'instanceMethod', 0)).toBe('test');
});

test('multiple', () => {
    class Target {
        public instanceMethod(
            @PathParameter('foo') instanceMethodParameter1: unknown,
            @PathParameter('bar') instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(PathParameters.TOKEN);
    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(PathParameters.TOKEN);

    expect(Metadata.get(MetadataKey.KEY, Target.prototype, 'instanceMethod', 0)).toBe('foo');
    expect(Metadata.get(MetadataKey.KEY, Target.prototype, 'instanceMethod', 1)).toBe('bar');
});
