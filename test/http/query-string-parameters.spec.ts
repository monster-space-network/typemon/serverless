import { Metadata } from '@typemon/reflection';
import { MetadataKey } from '@typemon/dependency-injection';
//
import { QueryStringParameters } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @QueryStringParameters() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(QueryStringParameters.TOKEN);
});

test('multiple', () => {
    class Target {
        public instanceMethod(
            @QueryStringParameters() instanceMethodParameter1: unknown,
            @QueryStringParameters() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(QueryStringParameters.TOKEN);
    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(QueryStringParameters.TOKEN);
});
