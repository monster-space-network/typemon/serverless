import { Metadata } from '@typemon/reflection';
import { MetadataKey } from '@typemon/dependency-injection';
//
import { HttpHeaders } from '../../src/http';
import { Headers } from '../../src';
//
//
//
test('initialize:record', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar'
    });

    expect(headers.get('foo')).toBe('bar');
    expect(headers.has('bar')).toBeFalsy();
});
test('initialize:array', () => {
    const headers: HttpHeaders = new HttpHeaders([
        ['foo', 'bar']
    ]);

    expect(headers.get('foo')).toBe('bar');
    expect(headers.has('bar')).toBeFalsy();
});
test('initialize:iterable', () => {
    const headers: HttpHeaders = new HttpHeaders(new Map().set('foo', 'bar'));

    expect(headers.get('foo')).toBe('bar');
    expect(headers.has('bar')).toBeFalsy();
});

test('iterator', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar',
        'bar': 'foo'
    });
    const arrayifiedHeaders: ReadonlyArray<[string, string]> = Array.from(headers);

    expect(arrayifiedHeaders).toMatchObject([
        ['foo', 'bar'],
        ['bar', 'foo']
    ]);
});

test('has', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar'
    });

    expect(headers.has('foo')).toBeTruthy();
    expect(headers.hasNot('foo')).toBeFalsy();
    expect(headers.has('bar')).toBeFalsy();
    expect(headers.hasNot('bar')).toBeTruthy();
});

test('get', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar'
    });

    expect(headers.get('foo')).toBe('bar');
    expect(() => headers.get('bar')).toThrow('Name does not exist.');
});

test('set', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar'
    });

    headers.set('foo', 'bar-changed');
    headers.set('bar', 'foo');

    expect(headers.get('foo')).toBe('bar-changed');
    expect(headers.get('bar')).toBe('foo');
});

test('delete', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar'
    });

    headers.delete('foo');

    expect(headers.has('foo')).toBeFalsy();
});

test('clear', () => {
    const headers: HttpHeaders = new HttpHeaders({
        'foo': 'bar'
    });

    headers.clear();

    expect(headers.has('foo')).toBeFalsy();
});

describe('decoratoration', () => {
    test('single', () => {
        class Target {
            public instanceMethod(
                @Headers() instanceMethodParameter: unknown
            ) { }
        }

        expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpHeaders);
    });

    test('multiple', () => {
        class Target {
            public instanceMethod(
                @Headers() instanceMethodParameter1: unknown,
                @Headers() instanceMethodParameter2: unknown
            ) { }
        }

        expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpHeaders);
        expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(HttpHeaders);
    });
});
