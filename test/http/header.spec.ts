import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../../src/metadata-key';
import { HttpHeaders } from '../../src/http';
import { Header } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @Header('test') instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpHeaders);
    expect(Metadata.get(MetadataKey.NAME, Target.prototype, 'instanceMethod', 0)).toBe('test');
});

test('multiple', () => {
    class Target {
        public instanceMethod(
            @Header('test') instanceMethodParameter1: unknown,
            @Header('test') instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpHeaders);
    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(HttpHeaders);

    expect(Metadata.get(MetadataKey.NAME, Target.prototype, 'instanceMethod', 0)).toBe('test');
    expect(Metadata.get(MetadataKey.NAME, Target.prototype, 'instanceMethod', 1)).toBe('test');
});
