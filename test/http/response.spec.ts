import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { HttpResponse } from '../../src/http';
import { Response } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @Response() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpResponse);
});
test('multiple', () => {
    class Target {
        public instanceMethod(
            @Response() instanceMethodParameter1: unknown,
            @Response() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpResponse);
    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(HttpResponse);
});
