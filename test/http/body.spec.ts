import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { Body } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @Body() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Body.TOKEN);
});

test('multiple', () => {
    class Target {
        public instanceMethod(
            @Body() instanceMethodParameter1: unknown,
            @Body() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Body.TOKEN);
    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(Body.TOKEN);
});
