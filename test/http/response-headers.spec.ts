import { Metadata } from '@typemon/reflection';
//
import { MetadataKey } from '../../src/metadata-key';
import { Response } from '../../src';
//
//
//
test('default', () => {
    class Target {
        @Response.Headers({
            'foo': 'bar'
        })
        public instanceMethod(): void { }
    }

    expect(Metadata.get(MetadataKey.Response.HEADERS, Target.prototype, 'instanceMethod')).toHaveProperty('foo', 'bar');
});

test('duplicate', () => {
    expect(() => {
        class Target {
            @Response.Headers({
                'foo': 'bar'
            })
            @Response.Headers({
                'bar': 'foo'
            })
            public instanceMethod(): void { }
        }
    }).toThrow('This decorator cannot be used in duplicate.');
});
