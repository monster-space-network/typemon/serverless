import { Metadata } from '@typemon/reflection';
//
import { MetadataKey } from '../../src/metadata-key';
import { Response } from '../../src';
//
//
//
test('invalid-status-code', () => {
    expect(() => {
        class Target {
            @Response.StatusCode(NaN)
            public instanceMethod(): void { }
        }
    }).toThrow('Invalid status code.');

    expect(() => {
        class Target {
            @Response.StatusCode(Infinity)
            public instanceMethod(): void { }
        }
    }).toThrow('Invalid status code.');

    expect(() => {
        class Target {
            @Response.StatusCode(4.04)
            public instanceMethod(): void { }
        }
    }).toThrow('Invalid status code.');

    expect(() => {
        class Target {
            @Response.StatusCode(99)
            public instanceMethod(): void { }
        }
    }).toThrow('Invalid status code.');

    expect(() => {
        class Target {
            @Response.StatusCode(512)
            public instanceMethod(): void { }
        }
    }).toThrow('Invalid status code.');
});

test('default', () => {
    class Target {
        @Response.StatusCode(404)
        public instanceMethod(): void { }
    }

    expect(Metadata.get(MetadataKey.Response.STATUS_CODE, Target.prototype, 'instanceMethod')).toBe(404);
});

test('duplicate', () => {
    expect(() => {
        class Target {
            @Response.StatusCode(404)
            @Response.StatusCode(404)
            public instanceMethod(): void { }
        }
    }).toThrow('This decorator cannot be used in duplicate.');
});
