import { HttpParameters } from '../../src/http';
//
//
//
test('initialize:record', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar'
    });

    expect(parameters.get('foo')).toBe('bar');
    expect(parameters.has('bar')).toBeFalsy();
});
test('initialize:array', () => {
    const parameters: HttpParameters = new HttpParameters([
        ['foo', 'bar']
    ]);

    expect(parameters.get('foo')).toBe('bar');
    expect(parameters.has('bar')).toBeFalsy();
});
test('initialize:iterable', () => {
    const parameters: HttpParameters = new HttpParameters(new Map().set('foo', 'bar'));

    expect(parameters.get('foo')).toBe('bar');
    expect(parameters.has('bar')).toBeFalsy();
});

test('iterator', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar',
        'bar': 'foo'
    });
    const arrayifiedParameters: ReadonlyArray<[string, string]> = Array.from(parameters);

    expect(arrayifiedParameters).toMatchObject([
        ['foo', 'bar'],
        ['bar', 'foo']
    ]);
});

test('has', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar'
    });

    expect(parameters.has('foo')).toBeTruthy();
    expect(parameters.hasNot('foo')).toBeFalsy();
    expect(parameters.has('bar')).toBeFalsy();
    expect(parameters.hasNot('bar')).toBeTruthy();
});

test('get', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar'
    });

    expect(parameters.get('foo')).toBe('bar');
    expect(() => parameters.get('bar')).toThrow('Key does not exist.');
});

test('set', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar'
    });

    parameters.set('foo', 'bar-changed');
    parameters.set('bar', 'foo');

    expect(parameters.get('foo')).toBe('bar-changed');
    expect(parameters.get('bar')).toBe('foo');
});

test('delete', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar'
    });

    parameters.delete('foo');

    expect(parameters.has('foo')).toBeFalsy();
});

test('clear', () => {
    const parameters: HttpParameters = new HttpParameters({
        'foo': 'bar'
    });

    parameters.clear();

    expect(parameters.has('foo')).toBeFalsy();
});
