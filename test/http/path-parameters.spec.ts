import { Metadata } from '@typemon/reflection';
import { MetadataKey } from '@typemon/dependency-injection';
//
import { PathParameters } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @PathParameters() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(PathParameters.TOKEN);
});

test('multiple', () => {
    class Target {
        public instanceMethod(
            @PathParameters() instanceMethodParameter1: unknown,
            @PathParameters() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(PathParameters.TOKEN);
    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(PathParameters.TOKEN);
});
