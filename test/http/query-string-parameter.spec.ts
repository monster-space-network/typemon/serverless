import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../../src/metadata-key';
import { QueryStringParameters } from '../../src/http';
import { QueryStringParameter } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @QueryStringParameter('test') instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(QueryStringParameters.TOKEN);
    expect(Metadata.get(MetadataKey.KEY, Target.prototype, 'instanceMethod', 0)).toBe('test');
});

test('multiple', () => {
    class Target {
        public instanceMethod(
            @QueryStringParameter('foo') instanceMethodParameter1: unknown,
            @QueryStringParameter('bar') instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(QueryStringParameters.TOKEN);
    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(QueryStringParameters.TOKEN);

    expect(Metadata.get(MetadataKey.KEY, Target.prototype, 'instanceMethod', 0)).toBe('foo');
    expect(Metadata.get(MetadataKey.KEY, Target.prototype, 'instanceMethod', 1)).toBe('bar');
});
