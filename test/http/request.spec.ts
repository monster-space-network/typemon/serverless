import { Metadata } from '@typemon/reflection';
import { MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { HttpRequest } from '../../src/http';
import { Request } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @Request() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpRequest);
});
test('multiple', () => {
    class Target {
        public instanceMethod(
            @Request() instanceMethodParameter1: unknown,
            @Request() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(HttpRequest);
    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(HttpRequest);
});
