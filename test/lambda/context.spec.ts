import { Metadata } from '@typemon/reflection';
import { MetadataKey } from '@typemon/dependency-injection';
//
import { Context } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @Context() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Context.TOKEN);
});
test('multiple', () => {
    class Target {
        public instanceMethod(
            @Context() instanceMethodParameter1: unknown,
            @Context() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Context.TOKEN);
    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(Context.TOKEN);
});
