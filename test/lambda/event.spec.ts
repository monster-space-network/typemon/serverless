import { Metadata } from '@typemon/reflection';
import { MetadataKey } from '@typemon/dependency-injection';
//
import { Event } from '../../src';
//
//
//
test('single', () => {
    class Target {
        public instanceMethod(
            @Event() instanceMethodParameter: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Event.TOKEN);
});
test('multiple', () => {
    class Target {
        public instanceMethod(
            @Event() instanceMethodParameter1: unknown,
            @Event() instanceMethodParameter2: unknown
        ) { }
    }

    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(Event.TOKEN);
    expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(Event.TOKEN);
});
