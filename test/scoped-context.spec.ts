import { Metadata } from '@typemon/reflection';
import { MetadataKey } from '@typemon/dependency-injection';
//
import { ScopedContext } from '../src';
import { ServerlessScopedContext } from '../src/scoped-context';
//
//
//
test('has', () => {
    const scopedContext: ServerlessScopedContext = new ServerlessScopedContext();

    scopedContext.set('foo', 'bar');

    expect(scopedContext.has('foo')).toBeTruthy();
    expect(scopedContext.hasNot('foo')).toBeFalsy();
    expect(scopedContext.has('bar')).toBeFalsy();
    expect(scopedContext.hasNot('bar')).toBeTruthy();
});

test('get', () => {
    const scopedContext: ServerlessScopedContext = new ServerlessScopedContext();

    scopedContext.set('foo', 'bar');

    expect(scopedContext.get('foo')).toBe('bar');
    expect(() => scopedContext.get('bar')).toThrow('Key does not exist.');
});

test('set', () => {
    const scopedContext: ServerlessScopedContext = new ServerlessScopedContext();

    scopedContext.set('foo', 'bar');
    scopedContext.set('foo', 'bar-changed');
    scopedContext.set('bar', 'foo');

    expect(scopedContext.get('foo')).toBe('bar-changed');
    expect(scopedContext.get('bar')).toBe('foo');
});

test('delete', () => {
    const scopedContext: ServerlessScopedContext = new ServerlessScopedContext();

    scopedContext.set('foo', 'bar');
    scopedContext.delete('foo');

    expect(scopedContext.has('foo')).toBeFalsy();
});

test('clear', () => {
    const scopedContext: ServerlessScopedContext = new ServerlessScopedContext();

    scopedContext.set('foo', 'bar');
    scopedContext.clear();

    expect(scopedContext.has('foo')).toBeFalsy();
});

describe('decoratoration', () => {
    test('single', () => {
        class Target {
            public instanceMethod(
                @ScopedContext() instanceMethodParameter: unknown
            ) { }
        }

        expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(ServerlessScopedContext);
    });
    test('multiple', () => {
        class Target {
            public instanceMethod(
                @ScopedContext() instanceMethodParameter1: unknown,
                @ScopedContext() instanceMethodParameter2: unknown
            ) { }
        }

        expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 0)).toBe(ServerlessScopedContext);
        expect(Metadata.get(MetadataKey.IDENTIFIER, Target.prototype, 'instanceMethod', 1)).toBe(ServerlessScopedContext);
    });
});
