import '../src';
import { HandlerParameterDecorator } from '../src/handler-parameter-decorator';
//
//
//
test('constructor-parameter', () => {
    expect(() => {
        class Target {
            public constructor(
                @HandlerParameterDecorator.create(() => { }) constructorParameter: unknown
            ) { }
        }
    }).toThrow('This decorator can only be used for instance method parameters.');
});

test('static-method-parameter', () => {
    expect(() => {
        class Target {
            public static staticMethod(
                @HandlerParameterDecorator.create(() => { }) staticMethodParameter: unknown
            ): void { }
        }
    }).toThrow('This decorator can only be used for instance method parameters.');
})

test('invalid-property-key', () => {
    expect(() => {
        class Target {
            public [Symbol.iterator](
                @HandlerParameterDecorator.create(() => { }) instanceMethodParameter: unknown
            ): void { }
        }
    }).toThrow('Invalid property key.');
});
