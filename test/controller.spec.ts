import { Metadata } from '@typemon/reflection';
import { InjectionToken, MetadataKey as DIMetadataKey } from '@typemon/dependency-injection';
//
import { MetadataKey } from '../src/metadata-key';
import { Controller, Middleware } from '../src';
//
//
//
test('default', () => {
    const options: Controller.Options = {
        http: false,
        providers: [{
            identifier: new InjectionToken('foo'),
            useValue: 'bar'
        }],
        middlewares: [class B implements Middleware { public handler(): void { } }]
    };

    @Controller(options)
    class Target {
        public constructor(
            constructorParameter1: string,
            constructorParameter2: number
        ) { }
    }

    expect(Metadata.get(MetadataKey.OPTIONS, Target)).toMatchObject(options);
    expect(Metadata.get(DIMetadataKey.TYPE, Target, 0)).toBe(String);
    expect(Metadata.get(DIMetadataKey.TYPE, Target, 1)).toBe(Number);
});

test('duplicate', () => {
    expect(() => {
        @Controller()
        @Controller()
        class Target { }
    }).toThrow('This decorator cannot be used in duplicate.');
});

test('metadata', () => {
    class Target {
        public handler(
            @Controller.Metadata() metadata: Metadata
        ): void { }
    }

    expect(Metadata.get(DIMetadataKey.IDENTIFIER, Target.prototype, 'handler', 0)).toBe(Controller.METADATA_TOKEN);
});
