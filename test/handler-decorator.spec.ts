import '../src';
import { HandlerDecorator } from '../src/handler-decorator';
//
//
//
test('static-method', () => {
    expect(() => {
        class Target {
            @HandlerDecorator.create(() => { })
            public static staticMethod(): void { }
        }
    }).toThrow('This decorator can only be used for instance methods.');
})

test('invalid-property-key', () => {
    expect(() => {
        class Target {
            @HandlerDecorator.create(() => { })
            public [Symbol.iterator](): void { }
        }
    }).toThrow('Invalid property key.');
});
